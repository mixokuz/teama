//
//  TeamEnterView.swift
//  teama
//
//  Created by Михаил on 17.10.2021.
//

import SwiftUI

struct TeamEnterView: View {
    @Binding var teamPlayers : [Player]
    @Binding var teamPlayersOther : [Player]
    @Binding var teamEntered: Bool
    var players : [Player]
    @Binding var teamName: String
    @State var playerName: String = ""
    @State var number: String = ""
    
    var savePlayer: (Player) -> ()
    var createTeam: (Team) -> ()
    var getTeamName: (Player) -> String
    var addPlayerToList: (Bool, Player) -> ()
    @State var height: CGFloat = 100
    var body: some View {
        VStack {
            if !teamEntered {
                TextField("teamName", text: $teamName)
                VStack {
                    ScrollView(.vertical) {
                        ForEach(players, id: \.id) { player in
                            HStack(spacing: 10) {
                                Text("\(player.number ?? 0)")
                                Text("\(player.firstName) \(player.lastName ?? "")")
                                Text(getTeamName(player))
                                Spacer()
                            }
                            .padding(10)
                            .onTapGesture {
                                if getTeamName(player) == "" {
                                    addPlayerToList(teamName == "team1" ? true : false, player)
                                } else {
                                    addPlayerToList(teamName == "team2" ? true : false, player)
                                }
                                
                            }
                        }
                    }
                }
                .frame(height: height)
                
                Button(action: {
                    height = height == 100 ? 500 : 100
                }) {
                    SmallButton(text: "Show more")
                }
                PlayerEnterView(playerName: "", numberString: "", savePlayer: savePlayer)
                HStack {
                    Button(action: {
                        if teamPlayers.count > 0 {
                            let team = Team(id: UUID().hashValue, name: teamName, players: teamPlayers, city: nil, country: nil, url: nil)
                            createTeam(team)
                            teamEntered.toggle()
                        }
                    }) {
                        SmallButton(text: "Save team", color: teamPlayers.count > 0 ? .green : .gray)
                    }
                }
            } else {
                TeamView(teamName: teamName, players: teamPlayers)
            }
        }
    }
}

struct TeamView: View {
    var teamName: String
    var players: [Player]
    
    var body: some View {
        VStack(alignment: .leading, spacing: 15) {
            Text(teamName)
                .font(.title)
            ForEach(players, id: \.id) { player in
                HStack {
                    Text("\(player.number ?? 0)")
                    Text(player.firstName)
                    Spacer()
                }
            }
        }
        .background(Color.red.opacity(0.2))
        .cornerRadius(15)
    }
}

//struct TeamEnterView_Previews: PreviewProvider {
//    static var previews: some View {
//        TeamEnterView(chosenPlayers: $chosenPlayers, players: <#Binding<[Player]>#>, savePlayer: <#(Player) -> ()#>)
//    }
//}
