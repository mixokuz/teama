//
//  Team.swift
//  teama
//
//  Created by Михаил on 11.10.2021.
//

import Foundation

struct Team: Hashable, Codable {
    var id: Int?
    var name: String
    var players: [Player] = [Player]()
    let city: String?
    let country: String?
    let url: String?
    
    enum CodingKeys: String, CodingKey {
        case name, city, country, url, id
    }
}

extension Team {
    init(teamDB: TeamDB) {
        id = teamDB.id
        name = teamDB.name
        players = teamDB.players.map(Player.init)
        city = nil
        country = nil
        url = nil
    }
}
