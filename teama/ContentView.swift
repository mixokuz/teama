//
//  ContentView.swift
//  teama
//
//  Created by Михаил on 06.10.2021.
//

import SwiftUI

struct ContentView: View {
    @StateObject var viewModel = GameViewModel()
    
//    var players: [Player] {
//        viewModel.players
//    }
//    @State var chosenPlayers = [Player]()
//    @State var chosenTeams = [Team]()
//    
//    @State var teamName: String = ""
//    @State var playerName: String = ""
//    @State var number: String = ""
//    @State var showPlayers: Int? = nil
    @State var isGameGoing: Bool = false
    
    var body: some View {
        VStack {
            if isGameGoing {
                GameScreenView(isGameGoing: $isGameGoing)
            } else {
                FirstScreenView(isGameGoing: $isGameGoing)
            }
        }
        .onAppear() {
            isGameGoing = viewModel.isGameGoing
            print(isGameGoing, "isGameGoing")
        }
        .onChange(of: viewModel.isGameGoing) { newValue in
            isGameGoing = newValue
            print(isGameGoing, "isGameGoing")
        }
        
        
        
//            VStack {
//                HStack {
//                    Button(action: {
//                        if chosenTeams.count == 2 {
//                            viewModel.startGame(teamFirst: chosenTeams[0], teamSecond: chosenTeams[1])
//                        }
//                    }) {
//                        Text("Start game")
//                    }
////                    if let game = viewModel.games.last {
//                        if viewModel.currentGame != nil && viewModel.currentGame!.teams.first != nil && viewModel.currentGame!.teams.last != nil {
//                            Button(action: {
//                                viewModel.addGoal(game: viewModel.currentGame!, teamID: viewModel.currentGame!.teams.first!.id)
//                            }) {
//                                Text("\(viewModel.currentGame!.teams.first!.name), \(viewModel.getGameGoal(game: viewModel.currentGame!, teamId: viewModel.currentGame!.teams.first!.id))")
//                            }
//                            Button(action: {
//                                viewModel.addGoal(game: viewModel.currentGame!, teamID: viewModel.currentGame!.teams.last!.id)
//                            }) {
//                                Text("\(viewModel.currentGame!.teams.last!.name), \(viewModel.getGameGoal(game: viewModel.currentGame!, teamId: viewModel.currentGame!.teams.last!.id))")
//                            }
//                        }
////                    }
//
//                }
//
//                ScrollView(.vertical) {
//                    VStack {
//                ForEach(viewModel.teams, id: \.id) { team in
//                    VStack {
//                        HStack {
//                            Text("\(team.name), \(team.players.count), \(chosenTeams.contains(where: { $0.id == team.id}) ? "chosen" : "")")
//                            Image(systemName: "multiply.circle")
//                                .onTapGesture {
//                                    viewModel.removeTeam(team: team)
//                                }
//                        }
//
//                        ForEach(team.players, id:  \.id) { player in
//                            Text("\(player.name), \(player.number)")
//                        }
//                    }
//                    .onTapGesture {
//                        if let teamIndex = chosenTeams.firstIndex(where: {$0.id == team.id}) {
//                            chosenTeams.remove(at: teamIndex)
//                            print("removed team  \(chosenTeams[teamIndex])")
//                        } else {
//                            chosenTeams.append(team)
//                            print("player \(team.name)")
//                        }
//
//                    }
//                }
//                ForEach(viewModel.players, id: \.id) { player in
//                    HStack {
//                        Text("\(player.name) \(player.number), \(chosenPlayers.contains(where: { $0.id == player.id}) ? "chosen" : "")")
//                            .frame(height: 30)
//                            .onTapGesture {
//                                if let playerIndex = chosenPlayers.firstIndex(where: {$0.id == player.id}) {
//    //                            if chosenPlayers.contains(where: {$0.id == player.id}) {
//                                    chosenPlayers.remove(at: playerIndex)
//                                    print("removed player  \(players[playerIndex])")
//                                } else {
//                                    chosenPlayers.append(player)
//                                    print("player \(player.name)")
//                                }
//
//                            }
//                        Image(systemName: "multiply.circle")
//                            .onTapGesture {
//                                viewModel.removePlayer(player: player)
//                            }
//                    }
//
//                }
//                }
//                }
//                VStack {
//                    TextField("teamName", text: $teamName)
//                        .background(Color.red.opacity(0.2))
//                    TextField("playerName", text: $playerName)
//                        .background(Color.green.opacity(0.2))
//                    TextField("numberPlayer", text: $number)
//                        .background(Color.blue.opacity(0.2))
//                }
//                HStack {
//                    Button(action: {
//                        let player = Player(id: UUID().hashValue, name: playerName, number: Int(number) ?? 0)
//                        viewModel.savePlayer(player: player)
//                        print("Button tapped")
//                    }) {
//                        Text("Add player")
//                    }
//                    Button(action: {
//                        if !teamName.isEmpty {
//                            viewModel.createTeam(team: Team(id: UUID().hashValue, name: teamName, players: chosenPlayers))
//                            chosenPlayers.removeAll()
//                        }
//                    }) {
//                        Text("Team")
//                    }
//                }
//                .padding(.horizontal)
//                Button(action: {
//                    viewModel.removeEverything()
//                }) {
//                    Text("Remove ALL")
//                }
//
//
//        }
    
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
