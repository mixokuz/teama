//
//  GameScreenView.swift
//  teama
//
//  Created by Михаил on 19.10.2021.
//

import SwiftUI

struct GameScreenView: View {
    @StateObject var viewModel = GameViewModel()
    
    let timer = Timer.publish(every: 60, on: .main, in: .common).autoconnect()
    @State var timeRemaining: Int = 0
    @Binding var isGameGoing: Bool
    
    var body: some View {
        if let game = viewModel.currentGame {
            let team1 = game.teams.first!
            let team2 = game.teams.last!
            VStack {
                HStack {
                    Spacer()
                    Text("\(viewModel.getGameGoal(game: game, teamId: team1.id!)) : \(viewModel.getGameGoal(game: game, teamId: team2.id!))")
                        .font(.title)
                    Text("\(timeRemaining)")
                    Spacer()
                }
                HStack {
                    Text(team1.name)
                    Text(team2.name)
                }
                
                ScrollView(.vertical) {
                    
                    ForEach(0 ..< game.goals.count) { _ in
                    
                        HStack {
                            Text("3 : 4")
                            Text("team1 player")
                            
                        }
                    }
                    
                }
                HStack {
                    Button(action: {
                        viewModel.addGoal(game: game, teamID: team1.id!)
                    }) {
                        SmallButton(text: "Add goal \(team1.name)")
                    }
                    Button(action: {
                        viewModel.addGoal(game: game, teamID: team2.id!)
                    }) {
                        SmallButton(text: "Add goal \(team2.name)")
                    }
                }
                Button(action: {
                    viewModel.stopGame(game: game)
                    isGameGoing = false
                }) {
                    SmallButton(text: "End game")
                }
            }
            .padding()
            .onAppear() {
                timeRemaining = viewModel.getRemainTime(game: game)
            }
            .onReceive(timer) { _ in
                timeRemaining = viewModel.getRemainTime(game: game)
            }
        }
        
    }
}

struct GameScreenView_Previews: PreviewProvider {
    static var previews: some View {
        GameScreenView(isGameGoing: .constant(true))
    }
}
