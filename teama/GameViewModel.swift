//
//  GameViewModel.swift
//  teama
//
//  Created by Михаил on 06.10.2021.
//

import Foundation
import RealmSwift

class GameViewModel: ObservableObject {
    
    @Published var teams: [Team]
//    @Published var goals: [Goal]
    
    @Published var players = [Player]()
    @Published var games : [Game]
    @Published var team1 = Team(id: UUID().hashValue, name: "team1", players: [Player](), city: nil, country: nil, url: nil)
    @Published var team2 = Team(id: UUID().hashValue, name: "team2", players: [Player](), city: nil, country: nil, url: nil)
    
    @Published var kanyeQuote = ""
    
    init() {
        let realm = try? Realm()
        
//        self.goals = [Goal]()
        if let player = realm?.objects(PlayerDB.self) {
            var playersDB = realm!.objects(PlayerDB.self)
            self.players = playersDB.map(Player.init)
        } else {
            self.players = [Player]()
        }
        
        if let team = realm?.objects(TeamDB.self) {
            var teamsDB = realm!.objects(TeamDB.self)
            self.teams = teamsDB.map(Team.init)
        } else {
            self.teams = [Team]()
        }
       
        if let games = realm?.objects(GameDB.self) {
            var gamesDB = realm!.objects(GameDB.self)
            self.games = gamesDB.map(Game.init)
        } else {
            self.games = [Game]()
        }
        print("games: \(games.count)")
        
        if let isGoing = games.first(where: { $0.isGoing }) {
            currentGame = isGoing
            print("game is going: \(games.contains(where: { $0.isGoing }) ? true : false)")
        } else {
            print(games.count, "not found current game")
        }
    }
    
    var teamsEntered: Bool {
        if teams.count > 1 {
            return true
        } else {
            return false
        }
    }
    
    @Published var currentGame : Game?
    
    var isGameGoing: Bool {
        if currentGame != nil {
            return true
        } else {
            return false
        }
    }
    
    func getGameGoal(game: Game, teamId: Int) -> Int {
        if currentGame != nil {
            return currentGame!.goals.filter({ $0.teamId == teamId }).count
        } else {
            return 0
        }
    }
    
    func getRemainTime(game: Game) -> Int {
        if currentGame != nil {
            let now = Date()
            let date = currentGame!.date
            if Calendar.current.component(.day, from: now) == Calendar.current.component(.day, from: date) {
                let diff = Int((Calendar.current.component(.hour, from: date) - Calendar.current.component(.hour, from: now)) * 60 + (Calendar.current.component(.minute, from: date) - Calendar.current.component(.minute, from: now)))
                
                return -diff
            }
        }
        return 0
    }
    
    func addPlayerToList(addToFirst: Bool, player: Player) {
        var list = addToFirst ? team1.players : team2.players
        var otherList = !addToFirst ? team1.players : team2.players
        if otherList.contains(player) {
            otherList.removeAll(where: { player.id == $0.id })
            print("Player \(player.firstName) \(player.lastName ?? "") removed from the \(!addToFirst ? "first" : "second") list ")
        }
        if !list.contains(player) {
            list.append(player)
            print("Player \(player.firstName) \(player.lastName ?? "") added to the  ")
        } else {
            list.removeAll(where: { player.id == $0.id})
            print("Player \(player.firstName) removed from \(addToFirst ? "first" : "second") list ")
        }
        team1.players = addToFirst ? list : otherList
        team2.players = !addToFirst ? list : otherList
        objectWillChange.send()
    }
    
    func addPlayerToTeam(teamId: Int, player: Player) {
        if let teamIndex = teams.firstIndex(where: {$0.id == teamId}) {
            let teamPlayers = teams[teamIndex].players
                if teamPlayers.contains(where: {$0.id == player.id}) {
                    teams[teamIndex].players.append(player)
                    print("Player \(player.firstName) added to the team \(teams[teamIndex].name)")
                } else {
                    print("Player \(player.firstName) already in team \(teams[teamIndex].name)")
                }
//            }
        }
        objectWillChange.send()
    }
    
    func removeEverything() {
        do {
            let realm = try Realm()
            try realm.write {
                realm.deleteAll()
            }
            var teamsDB = realm.objects(TeamDB.self)
            teams = teamsDB.map(Team.init)
            var playersDB = realm.objects(PlayerDB.self)
            players = playersDB.map(Player.init)
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func createTeam(team: Team) {
        if !teams.contains(where: {$0.id == team.id}) {
            teams.append(team)
            print("teams count", teams.count)
            saveTeam(team: team)
        }
        objectWillChange.send()
    }
    
    func saveTeam(team: Team) {
        if team.players != nil && team.id != nil {
            do {
                let realm = try Realm()
                let teamDB = TeamDB()
                teamDB.id = team.id!
                teamDB.name = team.name
                if let pl = realm.objects(PlayerDB.self).first {
                    var playersDB = realm.objects(PlayerDB.self)
                    
                    for player in team.players {
                        if let playerDB = playersDB.first(where: {player.id == $0.id }) {
                            teamDB.players.append(playerDB)
                        }
                    }
                }
                try realm.write {
                    realm.add(teamDB)
                }
                var teamsDB = realm.objects(TeamDB.self)
                teams = teamsDB.map(Team.init)
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    func removeTeam(team: Team) {
        do {
            let realm = try Realm()
            var teamsDB = realm.objects(TeamDB.self)
            if let teamDB = teamsDB.first(where: {$0.id == team.id}) {
                try realm.write {
                    realm.delete(teamDB)
                }
            }
            teams = teamsDB.map(Team.init)
           
        } catch {
            print(error.localizedDescription)
        }
        objectWillChange.send()
    }
    
    func createPlayer(player: Player) {
        if !players.contains(where: {$0.id == player.id || ($0.firstName == player.firstName && $0.lastName == player.lastName)}) {
            players.append(player)
            savePlayer(player: player)
        }
    }
    
    func savePlayer(player: Player) {
        if !players.contains(where: {$0.id == player.id || ($0.firstName == player.firstName && $0.lastName == player.lastName)}) {
            do {
                let realm = try Realm()
                
                var playerDB = PlayerDB()
                playerDB.id = player.id
                playerDB.name = player.firstName
                playerDB.number = player.number ?? 0
//                playerDB.games = player.games
                
                try realm.write {
                    realm.add(playerDB)
                    print("playerDB")
                }
                
                var playersDB = realm.objects(PlayerDB.self)
                players = playersDB.map(Player.init)
                print(players.count)
                
            } catch let error {
                print(error.localizedDescription)
            }
            
        } else {
            print("Player \(player.id) alreadt exist")
        }
        objectWillChange.send()
    }
    
    func removePlayer(player: Player) {
        do {
            let realm = try Realm()
            var playersDB = realm.objects(PlayerDB.self)
            if let playerDB = playersDB.first(where: {$0.id == player.id}) {
                try realm.write {
                    realm.delete(playerDB)
                }
            }
            players = playersDB.map(Player.init)
           
        } catch let error {
            print(error.localizedDescription)
        }
        objectWillChange.send()
    }
//    MARK: getTeamName
    func getTeamName(for player: Player) -> String {
        if team1.players.contains(player) {
            return team1.name
        } else if team2.players.contains(player) {
            return team2.name
        } else {
            return ""
        }
    }
    //    MARK: saveGame
    func saveGame(game: Game) {
        var gameDB = GameDB()
        gameDB.id = game.id
        gameDB.date = game.date
        gameDB.isGoing = true
        //        gameDB.goals = RealmSwift.List<GoalDB>()
                
        do {
            let realm = try Realm()
            var teamsDB = realm.objects(TeamDB.self)
            let teamFirstDB = teamsDB.first(where: {$0.id == game.teams.first!.id})
            let teamSecondDB = teamsDB.first(where: {$0.id == game.teams.last!.id})
            gameDB.teams.append(teamFirstDB!)
            gameDB.teams.append(teamSecondDB!)
            
            try realm.write {
                realm.add(gameDB)
                print("gameDB created")
                games = realm.objects(GameDB.self).map(Game.init)
            }
        } catch let error {
            print(error.localizedDescription)
        }
        
    }
    
    func startGame(teamFirst: Team, teamSecond: Team) {
        var game = Game(id: UUID().hashValue, teams: [teamFirst, teamSecond], date: Date(), goals: [Goal](), isGoing: true)
        games.append(game)
        print(games.count, "games.count")
        saveGame(game: game)
        currentGame = games.last
        
    }
    
    func deleteGame(game: Game) {
        do {
            let realm = try Realm()
            var gamesDB = realm.objects(GameDB.self)
            if let gameDB = gamesDB.first(where: { $0.id == game.id }) {
                try realm.write {
                    realm.delete(gameDB)
                }
            }
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func stopGame(game: Game) {
        var currentGame = games.first(where: { $0.isGoing })
        
        if  currentGame != nil {
            currentGame!.isGoing = false
            print("currentGame finished", currentGame!.id)
            currentGame = nil
        }
        
        do {
            let realm = try Realm()
            var gamesDB = realm.objects(GameDB.self)
            
            if let gameDB = gamesDB.first(where: { $0.id == game.id }) {
                try realm.write {
                    gameDB.isGoing = false
                }
//                games = realm.objects(GameDB.self).map(Game.init)
                self.games = gamesDB.map(Game.init)
//                self.teams
//                gameDB.isGoing = false
//                try realm.write {
//                    realm.add(gameDB)
//                }
            }
        } catch let error {
            print(error.localizedDescription)
        }
        objectWillChange.send()
    }
    
    func addGoal(game: Game, teamID: Int) {
        let goal = Goal(id: UUID().hashValue, teamId: teamID, order: game.goals.count)
        if let gameIndex = games.firstIndex(where: { game.id == $0.id }) {
            games[gameIndex].goals.append(goal)
            
            var goalDB = GoalDB()
            goalDB.id = goal.id
            goalDB.order = goal.order
            goalDB.teamId = teamID
            
            do {
                let realm = try Realm()
                let gamesDB = realm.objects(GameDB.self)
                if let gameDB = gamesDB.first(where: { $0.id == game.id }) {
                    try realm.write {
                        gameDB.goals.append(goalDB)
                        
                    }
                    
                }
                games = realm.objects(GameDB.self).map(Game.init)
                
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
//    MARK: fetchPlayer function
    
    func fetchPlayer() {
        guard let url = URL(string: "https://175g.ru/api/v1/players/registered") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: url) { [weak self]
             data, _, error in
            guard let data = data, error == nil else {
                return
            }
            
//            convert to JSON
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let data = try decoder.decode(PlayerResponse.self, from: data)
                print(data.data.count)
//                let players = try JSONDecoder().decode([PlayerD].self, from: data)
                    DispatchQueue.main.async {
                        var playersD = data.data.filter({ $0.teams?.firstIndex(where: {$0.name == "Рэмпейдж"}) != nil })
                        for player in playersD {
                            let playerNew = Player(id: UUID().hashValue, number: player.number, firstName: player.firstName, lastName: player.lastName, sex: player.sex, url: player.url, teams: [])
                            self?.createPlayer(player: playerNew)
                        }
                        print(self?.players.count, "players.count")
                    }
            } catch {
                print(error)
            }
        }
        
        task.resume()
    }
    
    func fetchKanye() {
        
        let url = URL(string: "https://api.kanye.rest/")!
        
        let task = URLSession.shared.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            if let error = error {
                print("Error", error)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                print("not the right response")
                return
            }
            
            guard (200...299).contains(httpResponse.statusCode) else {
                print("Error, status code", httpResponse.statusCode)
                return
            }
            
            guard let data = data else {
                print("bad data")
                return
            }
            
            do {
                let json = try! JSONSerialization.jsonObject(with: data, options: []) as! [String: String]
                
                DispatchQueue.main.async {
                    self.kanyeQuote = json["quote"] ?? ""
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        
        task.resume()
        
    }
}

struct PlayerResponse: Hashable, Decodable {
    var data : [Player]
}


