//
//  PlayerEnterView.swift
//  teama
//
//  Created by Михаил on 17.10.2021.
//

import SwiftUI
import RealmSwift

struct PlayerEnterView: View {
    @State var playerName: String
    @State var numberString: String 
    var savePlayer: (Player) -> ()
    @State var showField: Bool = false
    var body: some View {
        VStack {
            Button(action: {
                showField.toggle()
            }) {
                SmallButton(text: "Add player")
            }
        }
        if showField {
            HStack {
                TextField("number", text: $numberString)
                .keyboardType(.decimalPad)
                .frame(width: 100)
                TextField("playerName", text: $playerName)
                Button(action: {
                    if playerName != "" && Int(numberString) != nil {
                        let player = Player(id: UUID().hashValue, number: Int(numberString)!, firstName: playerName, lastName: "", sex: 1, url: "", teams: [])
                        savePlayer(player)
                        playerName = ""
                        numberString = ""
                    }
                }) {
                    SmallButton(text: "Save")
                }
            }
            .frame(height: 50)
            .background(Color.red.opacity(0.2))
            .cornerRadius(15)
        }
        
    }
}
