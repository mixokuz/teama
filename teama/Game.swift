//
//  game.swift
//  teama
//
//  Created by Михаил on 06.10.2021.
//

import Foundation

struct Game {
    var id: Int
    var teams: [Team]
    var date: Date
    var goals: [Goal]
    var isGoing: Bool
}

extension Game {
    init(gameDB: GameDB) {
        id = gameDB.id
        teams = gameDB.teams.map(Team.init)
        date = gameDB.date
        goals = gameDB.goals.map(Goal.init)
        isGoing = gameDB.isGoing
    }

}

struct Goal {
    var id: Int
//    var gameId: Int
    var teamId: Int
    var order: Int
//    var playerAssist: [Player]
//    var playerGoal: Player
}

extension Goal {
    init(goalDB: GoalDB) {
        id = goalDB.id
//        gameId = goalDB.gameId
        teamId = goalDB.teamId
//        playerGoal = Player(playerDB: goalDB.playerGoal)
//        playerAssist = goalDB.playerAssist.map(Player.init)
        order = goalDB.order
    }
}


