//
//  First Screen View.swift
//  teama
//
//  Created by Михаил on 16.10.2021.
//

import SwiftUI

struct FirstScreenView: View {
    @StateObject var viewModel = GameViewModel()
    
    @State var startingNewGame = true
    
    @State var team1Entered : Bool = false
    @State var team2Entered : Bool = false
    
    @Binding var isGameGoing: Bool
    var body: some View {
        ScrollView(.vertical) {
            VStack {
                Button(action: {
                    viewModel.fetchKanye()
                }) {
                    VStack {
                        Text(viewModel.kanyeQuote)
                        SmallButton(text: "Kanye quote for real")
                    }
                }
                TeamEnterView(teamPlayers: $viewModel.team1.players, teamPlayersOther: $viewModel.team2.players, teamEntered: $team1Entered, players: viewModel.players, teamName: $viewModel.team1.name,  savePlayer: viewModel.createPlayer, createTeam: viewModel.createTeam, getTeamName: viewModel.getTeamName, addPlayerToList: viewModel.addPlayerToList)
                TeamEnterView(teamPlayers: $viewModel.team2.players,teamPlayersOther: $viewModel.team1.players, teamEntered: $team2Entered, players: viewModel.players, teamName: $viewModel.team2.name, savePlayer: viewModel.createPlayer, createTeam: viewModel.createTeam, getTeamName: viewModel.getTeamName, addPlayerToList: viewModel.addPlayerToList)
            }
            HStack {
                Button(action: {
                    viewModel.fetchPlayer()
                }) {
                    SmallButton(text: "Download 175g")
                }
                if viewModel.teams.count > 2 {
                    Button(action: {
                        viewModel.startGame(teamFirst: viewModel.teams.first!, teamSecond: viewModel.teams.last!)
                        isGameGoing = true
                    }) {
                        SmallButton(text: "Start game")
                    }
                }
                Button(action: {
                    viewModel.removeEverything()
                }) {
                    SmallButton(text: "remove all")
                }
            }
            
        }
        .onAppear() {
            viewModel.fetchPlayer()
        }
        .padding()
        .background() {
            Color.red.opacity(0.2)
        }
        .cornerRadius(20)
        .padding(.horizontal)
        
    }
}

struct First_Screen_View_Previews: PreviewProvider {
    static var previews: some View {
        FirstScreenView(isGameGoing: .constant(false))
    }
}


struct PlayersFrom175List: View {
    @Binding var showList: Bool
    var playersD: [Player]
    @Binding var teamPlayers: [Player]
    var body: some View {
        if showList {
            ForEach(playersD, id:  \.self) { player in
                HStack {
                    Text(player.firstName)
                    Text(player.lastName ?? "")
                    if teamPlayers.contains(where: { $0.firstName == player.firstName}) {
                        Text("+")
                    }
                }
                .onTapGesture {
                    if !teamPlayers.contains(where: {$0.firstName == player.firstName}) {
                        var playerToAdd = Player(id: UUID().hashValue, number: 22, firstName: player.firstName, lastName: player.lastName, sex: player.sex, url: player.url, teams: [])
                        
                        teamPlayers.append(playerToAdd)
                    } else {
                        teamPlayers.removeAll(where: {player.firstName == $0.firstName && player.lastName == $0.lastName})
                    }
                    
                }
            }
            .frame(height: 200)
        }
    }
}
