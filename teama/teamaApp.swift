//
//  teamaApp.swift
//  teama
//
//  Created by Михаил on 06.10.2021.
//

import SwiftUI

@main
struct teamaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
