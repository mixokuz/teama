//
//  Player.swift
//  teama
//
//  Created by Михаил on 07.10.2021.
//

import Foundation

struct Player: Hashable, Codable {
    var id: Int
    var number: Int?
    var firstName: String
    var lastName: String?
    let sex: Int?
    let url: String?
    var teams: [Team]?
    
    enum CodingKeys: String, CodingKey {
        case id, number, firstName, sex, teams, lastName, url
    }
}

extension Player {
    init(playerDB: PlayerDB) {
        id = playerDB.id
        number = playerDB.number
        firstName = playerDB.name
        sex = nil
        teams = nil
        lastName = nil
        url = nil
    }
}
