//
//  PlayersDB.swift
//  teama
//
//  Created by Михаил on 07.10.2021.
//

import RealmSwift
import Foundation

class PlayerDB: Object, ObjectKeyIdentifiable {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var number = 0
    @objc dynamic var firstName : String? = ""
    @objc dynamic var lastName: String? = ""
//    @objc dynamic var games = [0]
    
    override static func primaryKey() -> String? {
        "id"
    }
    
    var team = LinkingObjects(fromType: TeamDB.self, property: "players")
//    var playerGoal = LinkingObjects(fromType: GoalDB.self, property: "playerGoal")
//    var playerAssist = LinkingObjects(fromType: GoalDB.self, property: "playerAssist")
}

class TeamDB: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
//    connection to player
    var players = RealmSwift.List<PlayerDB>()
    
    override static func primaryKey() -> String? {
        "id"
    }
    var game = LinkingObjects(fromType: GameDB.self, property: "teams")
    
}

class GameDB: Object, ObjectKeyIdentifiable {
    @objc dynamic var id = 0
    
    override static func primaryKey() -> String? {
        "id"
    }
    
    @objc dynamic var date = Date()
    var teams = RealmSwift.List<TeamDB>()
    var goals = RealmSwift.List<GoalDB>()
    @objc dynamic var isGoing = false
//    var team1 = LinkingObjects(fromType: GameDB.self, property: "team")
}

class GoalDB: Object, ObjectKeyIdentifiable {
    @objc dynamic var id = 0
    override static func primaryKey() -> String? {
        "id"
    }
//    @objc dynamic var gameId = 0
    @objc dynamic var teamId = 0
    @objc dynamic var order = 0
    
    var game = LinkingObjects(fromType: GameDB.self, property: "goals")
//    var playerAssist = RealmSwift.List<PlayerDB>()
//    var playerGoal = PlayerDB()
//    @objc dynamic var playerAssist = PlayerDB()
//    @objc dynamic var playerGoal = PlayerDB()
    
}
