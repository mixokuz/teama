//
//  UIElements.swift
//  teama
//
//  Created by Михаил on 17.10.2021.
//

import SwiftUI

struct SmallButton: View {
    var text: String
    var color: Color = .green
    var body: some View {
        Text(text)
            .padding(10)
            .foregroundColor(.white)
            .background(Color.green)
            .cornerRadius(15)
    }
}

struct UIElements_Previews: PreviewProvider {
    static var previews: some View {
        SmallButton(text: "Save")
    }
}
